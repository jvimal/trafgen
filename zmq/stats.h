
#define BINS (4)
#define HIST_POWERS (4)
#define BUCKETS (10)
#define MIN_FCT_USEC (50.0)

struct Histogram {
	u64 count[HIST_POWERS][BUCKETS];
	u64 total_count;
	double start_del;

	Histogram(double start=1) {
		double curr = start, del = start;
		bzero(count, sizeof(count));
		total_count = 0;
		start_del = start;
	}

	void insert(double value) {
		int power = 0;
		while (not (0 <= value and value < 10)) {
			value /= 10;
			power++;
		}
		count[power][int(value)]++;
		total_count++;
	}

	void print() {
		double min_frac = 10;
		double frac[HIST_POWERS][BUCKETS];
		double sum = 0.0;
		double value = start_del;
		double del = start_del;

		REP(i, HIST_POWERS) {
			REP(j, BUCKETS) {
				if (count[i][j] == 0)
					continue;
				frac[i][j] = count[i][j] * 1.0/total_count;
				if (frac[i][j] > 1e-2)
					min_frac = min(min_frac, frac[i][j]);
			}
		}

		REP(i, HIST_POWERS) {
			REP(j, BUCKETS) {
				int num = min(20, int(frac[i][j]/min_frac));
				if (count[i][j] == 0 or num == 0)
					continue;
				sum += frac[i][j];
				printf("[%.3lf, %.3lf) => %.4lf, %.4lf  ",
				       value, value + del,
				       frac[i][j], sum);
				REP(j, num)
					printf("#");
				printf("\n");
				value += del;
			}
			del *= 10;
			value = del;
		}
	}

	void clear() {
		total_count = 0;
		bzero(count, sizeof(count));
	}
};

struct FCTStats {
	double rate_mbps;
	Histogram ratio_bins[BINS];
	Histogram fct_bins[BINS];
	u32 flowsize_cutoff[BINS];
	int num_cutoffs;

	FCTStats(double r=10000):rate_mbps(r) {
		flowsize_cutoff[0] = 100 * 1000;
		flowsize_cutoff[1] = 100 * 1000 * 1000;
		flowsize_cutoff[2] = ~0;
		num_cutoffs = 2;

		/* for FCTs start with 100us */
		REP(i, BINS) fct_bins[i].start_del = 100;
	}

	void insert(int flowsize, double fct_usec) {
		double best_usec = max(flowsize * 8. / rate_mbps, MIN_FCT_USEC);
		double ratio = fct_usec / best_usec;

		REP(i, num_cutoffs) {
			if (flowsize <= flowsize_cutoff[i]) {
				ratio_bins[i].insert(ratio);
				fct_bins[i].insert(fct_usec);
				break;
			}
		}
	}

	void print() {
		printf("----------- %d kB -----------\n", flowsize_cutoff[0]/1000);
		ratio_bins[0].print();
		printf("----------- %d kB -----------\n", flowsize_cutoff[1]/1000);
		ratio_bins[1].print();
	}

	void clear() {
		REP(i, num_cutoffs) {
			fct_bins[i].clear();
			ratio_bins[i].clear();
		}
	}
};
