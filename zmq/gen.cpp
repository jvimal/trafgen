#include <sys/time.h>
#include <stdio.h>
#include <thread>

using namespace std;

#include "zmq.hpp"
#include <zmq.h>
#include "random.h"

#define REP(i, n) for (int i = 0; i < n; ++i)
#define NSEC_PER_SEC (1000000000LLU)

typedef unsigned long long u64;
typedef unsigned long u32;

#include "stats.h"

FCTStats fctstats;

namespace config {
	int CONNECTIONS_PER_PAIR = 30;
	int NUM_WORKERS = 4;
	int SLEEP_SEC = 5;
	int EXITING = 0;
	const int BUFFER_SIZE = 128 * 1024;
	char BLOB[BUFFER_SIZE];
	const double MAX_FLOW_SIZE = 1000 * 1000 * 1000;
	const double MAX_INTER_TIME_USEC = 1000 * 1000;

	int TENANT_ID = -1;
	int PORT = 6655;
	int ID;
	int BASE_RTT_USEC=50;

	/* 1 MB */
	int MEAN_FLOW_SIZE = 1000 * 1000;
	int MEAN_RATE_MBPS = 4000;
};

namespace stats {
	u64 avg_nsec;
	u64 acks_sent;
	u64 acks_received;
	u64 msgs_sent;
};

double size_bytes;
double usec;

char identities[32][128] = {
};

int idlen[100];

struct msg_info {
	int bytes;
	struct timespec start;
};

char *listen_address() {
	static char listenaddr[128];
	sprintf(listenaddr, "tcp://%s:%d",
		identities[config::ID], config::PORT);
	return listenaddr;
}

u64 delta_nsec(struct timespec &start, struct timespec &end) {
	return (end.tv_sec - start.tv_sec) * NSEC_PER_SEC + (end.tv_nsec - start.tv_nsec);
}

u64 time_nsec() {
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	return t.tv_sec * NSEC_PER_SEC + t.tv_nsec;
}

char *get_addr(int id) {
        static char buff[100];
	sprintf(buff, "tcp://%s:%d", identities[id], config::PORT);
	return buff;
}

char *monitorfile() {
	static char buff[100];
	pid_t pid = getpid();
	sprintf(buff, "inproc://monitor.%d", pid);
	return buff;
}

void init(int argc, char *argv[]) {
	int i;
	int c;

	rand_init();

	while ((c = getopt(argc, argv, "T:n:i:r:P:")) != -1) {
		switch (c) {
		case 'T':
			config::TENANT_ID = atoi(optarg);
			break;

		case 'n':
			config::NUM_WORKERS = atoi(optarg);
			break;

		case 'i':
			config::ID = atoi(optarg);
			break;

		case 'r':
			config::MEAN_RATE_MBPS = atoi(optarg);
			break;

		case 'P':
			config::CONNECTIONS_PER_PAIR = atoi(optarg);
			break;
		}
	}

	REP(i, config::NUM_WORKERS) {
		if (config::TENANT_ID == -1) {
			sprintf(identities[i], "192.168.2.%d", i+1);
		} else {
			sprintf(identities[i], "11.0.%d.%d", config::TENANT_ID, i+1);
		}
		idlen[i] = strlen(identities[i]);
	}
}

static void monitor_thread(void *ctx) {
	zmq_event_t event;
	int rc;
	void *s = zmq_socket(ctx, ZMQ_PAIR);
	assert(s);

	zmq_connect(s, monitorfile());
	zmq_pollitem_t items[] = {
		{s, 0, ZMQ_POLLIN, 0},
	};

	while (true) {
		zmq_msg_t msg;
		zmq_msg_init (&msg);

		zmq_poll(items, 1, 1000);

		if (config::EXITING)
			break;

		printf("Average: %llu, %lluus, %.3fms; "
		       "acks_sent: %llu, acks_received: %llu "
		       "msgs_sent: %llu\n",
		       stats::avg_nsec, stats::avg_nsec/1000, stats::avg_nsec/1e6,
		       stats::acks_sent, stats::acks_received, stats::msgs_sent);

		fctstats.print();
		//fctstats.clear();

		if (!items[0].revents & ZMQ_POLLIN)
			continue;

		rc = zmq_recvmsg(s, &msg, 0);
		if (rc == -1 && zmq_errno() == ETERM) break;
		assert (rc != -1);
		memcpy (&event, zmq_msg_data(&msg), sizeof (event));

		switch (event.event) {
		case ZMQ_EVENT_LISTENING:
			//printf ("listening socket descriptor %d\n", event.data.listening.fd);
			printf ("listening socket address %s\n", event.data.listening.addr);
			break;

		case ZMQ_EVENT_ACCEPTED:
			//printf ("accepted socket descriptor %d\n", event.data.accepted.fd);
			printf ("accepted socket address %s\n", event.data.accepted.addr);
			break;

		case ZMQ_EVENT_CLOSE_FAILED:
			printf ("socket close failure error code %d\n", event.data.close_failed.err);
			printf ("socket address %s\n", event.data.close_failed.addr);
			break;

		case ZMQ_EVENT_CLOSED:
			//printf ("closed socket descriptor %d\n", event.data.closed.fd);
			printf ("closed socket address %s\n", event.data.closed.addr);
			break;

		case ZMQ_EVENT_DISCONNECTED:
			//printf ("disconnected socket descriptor %d\n", event.data.disconnected.fd);
			printf ("disconnected socket address %s\n", event.data.disconnected.addr);
			break;
		}

		zmq_msg_close (&msg);
	}
}

thread monitor_init(void *ctx, void *sock) {
	int rc = zmq_socket_monitor(sock, monitorfile(), ZMQ_EVENT_ALL);
	return thread(monitor_thread, ctx);
}

void receive_message(zmq::socket_t &_sock, zmq::socket_t &send_sock) {
	char from[128];
	int more;
	size_t sz = 4;
	void *sock = static_cast<void *>(_sock);
	void *ssock = static_cast<void *>(send_sock);
	int fromlen = zmq_recv(sock, from, 128, 0);
	from[fromlen] = 0;

	int bytes = 0;
	struct msg_info info;
	zmq_recv(sock, &info, sizeof(info), 0);

	/* If this is a message and not an ACK */
	if (info.bytes > 0) {
		do {
			int num = zmq_recv(sock, config::BLOB, config::BUFFER_SIZE, 0);
			zmq_getsockopt(sock, ZMQ_RCVMORE, &more, &sz);
			bytes += num;
		} while (more);

		/* Send an ACK */
		info.bytes = -info.bytes;
		zmq_send(send_sock, from, fromlen, ZMQ_SNDMORE);
		zmq_send(send_sock, &info, sizeof(info), 0);
		stats::acks_sent++;
	} else {
		int bytes = -info.bytes;
		u64 nsec;
		struct timespec end;
		clock_gettime(CLOCK_MONOTONIC, &end);
		nsec = delta_nsec(info.start, end);
		stats::avg_nsec = ((stats::avg_nsec * 15) + nsec) >> 4;
		stats::acks_received++;
		fctstats.insert(bytes, nsec/1000 - config::BASE_RTT_USEC/2);
	}
}

void send_message(zmq::socket_t &_sock, int destid, int bytes) {
	void *sock = static_cast<void *>(_sock);
	char *dest = identities[destid];
	int destlen = idlen[destid];
	int quotient = bytes / config::BUFFER_SIZE;
	int rem = bytes % config::BUFFER_SIZE;
	int num;
	struct msg_info info;

	/* Set the destination */
	num = zmq_send(sock, dest, destlen, ZMQ_SNDMORE);

	/* Message info */
	info.bytes = bytes;
	clock_gettime(CLOCK_MONOTONIC, &info.start);
	if (zmq_send(sock, &info, sizeof(info), ZMQ_SNDMORE) < 0) {
		perror("zmq_send");
	}

	/* Simplifies things */
	if (rem == 0) {
		rem = 100;
		bytes += rem;
	}

	while (quotient--) {
		num = zmq_send(sock, config::BLOB, config::BUFFER_SIZE, ZMQ_SNDMORE);
		if (num < 0)
			perror("zmq_send");
		assert(num > 0);
	}

	/* The last part */
	assert(zmq_send(sock, config::BLOB, rem, 0) > 0);
	stats::msgs_sent++;
}

int main(int argc, char *argv[]) {
	zmq::context_t context(4);
	zmq::socket_t sender(context, ZMQ_ROUTER);
	zmq::socket_t receiver(context, ZMQ_ROUTER);
	thread mon;
	init(argc, argv);

	mon = monitor_init(static_cast<void *>(context),
			   static_cast<void *>(sender));

	/* Connect to other hosts and set our identity. */
	printf("Setting identity: %s, len %d\n",
	       identities[config::ID],
	       idlen[config::ID]);
	zmq_setsockopt(static_cast<void *>(receiver),
		       ZMQ_IDENTITY,
		       identities[config::ID],
		       idlen[config::ID]);
	zmq_setsockopt(static_cast<void *>(sender),
		       ZMQ_IDENTITY,
		       identities[config::ID],
		       idlen[config::ID]);

	/* Sender waits for connections */
	sender.bind(listen_address());

	REP(i, config::NUM_WORKERS) {
		/* Don't connect to ourself */
		if (i == config::ID)
			continue;
		REP(j, config::CONNECTIONS_PER_PAIR) {
			receiver.connect(get_addr(i));
		}
	}

	printf("Waiting for a while...\n");
	/* Wait for a while... */
	sleep(config::SLEEP_SEC);

	zmq::pollitem_t items[] = {
		{ sender, 0, ZMQ_POLLIN | ZMQ_POLLOUT, 0 },
		{ receiver, 0, ZMQ_POLLIN, 0 },
	};

	double mean_inter_usec = config::MEAN_FLOW_SIZE * 8.0 / config::MEAN_RATE_MBPS;
	printf("Mean inter flow time: %.3lfus\n", mean_inter_usec);
	RandomExp interus(mean_inter_usec);
	//RandomPareto sizes(config::MEAN_FLOW_SIZE, 1.1);
	RandomExp sizes(config::MEAN_FLOW_SIZE);

	u64 next = time_nsec();
	int ready = 0;

	while (1) {
		/* Poll indefinitely */
		int ret = zmq::poll(items, 2, 0);
		u64 now = time_nsec();

		if (items[1].revents & ZMQ_POLLIN) {
			receive_message(receiver, sender);
		}

		if ((items[0].revents & ZMQ_POLLOUT)
		    and (now >= next))
		{
			int r;
			size_bytes = min(sizes.gen(), config::MAX_FLOW_SIZE);
			usec = min(interus.gen(), config::MAX_INTER_TIME_USEC);
			do { r = rand() % config::NUM_WORKERS; } while (r == config::ID);

			send_message(sender, r, size_bytes);
			next = now + usec * 1000;
		}
	}

	mon.join();
	return 0;
}
