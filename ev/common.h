
#ifndef __COMMON_H__
#define __COMMON_H__

#include <fcntl.h>
#include <unistd.h>

#define REP(i, n) for (int i = 0; i < n; ++i)
#define EACH(it, cont) for (auto it = (cont).begin(); it != (cont).end(); ++it)
#define NSEC_PER_SEC (1000000000LLU)
#define USEC_PER_SEC (1000000)

typedef unsigned long long u64;
typedef unsigned long u32;


static void set_non_blocking(int fd)
{
	fcntl(fd, F_SETFL,
	      fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
}

static void set_nice(int n)
{
	int ret;
	errno = 0;
	ret = nice(n);
	if (ret == -1 && errno) {
		perror("nice");
		exit(-1);
	} else if (ret != n) {
		perror("nice-");
		exit(-1);
	}
}

#endif /* __COMMON_H__ */
