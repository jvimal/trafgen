
#include <thread>
#include "common.h"
#include "random.h"

namespace config {
	int CONNECTIONS_PER_PAIR = 2;
	int NUM_WORKERS = 4;
	int SLEEP_SEC = 5;
	int EXITING = 0;
	const int BUFFER_SIZE = 128 * 1024;
	char BLOB[BUFFER_SIZE];
	const double MAX_FLOW_SIZE = 1000 * 1000 * 1000;
	const double MAX_INTER_TIME_USEC = 1000 * 1000;

	int TENANT_ID = -1;
	int PORT = 6655;
	int ID;
	int BASE_RTT_USEC=50;

	/* 1 MB */
	int MEAN_FLOW_SIZE = 1000 * 1000;
	int MEAN_RATE_MBPS = 4000;

	int TOS_CUTOFF = 0;
	int MAX_WORK_BACKLOG = 1000000;

	int VERBOSE_LEVEL = 0;

	int NICE_LEVEL = -20;
};

namespace stats {
	u64 avg_nsec;
	u64 acks_sent;
	u64 acks_received;
	u64 msgs_sent;

	u64 msgs_dropped;
	u64 bytes_dropped;
};

#include "stats.h"
#include "server.cpp"
#include "client.cpp"

Server *server;
Client *client;
char hosts[32][128];
FCTStats fctstats;

void usage(char *p) {
	printf("%s: -TnirPtmh\n", p);
	printf("\tT: Current worker's tenant ID (%d)\n", config::TENANT_ID);
	printf("\tn: Number of workers (machines) (%d).\n", config::NUM_WORKERS);
	printf("\ti: Current worker's ID (0 -- n-1) (%d)\n", config::ID);
	printf("\tr: Mean rate in Mb/s (%d)\n", config::MEAN_RATE_MBPS);
	printf("\tP: Connections per pair of workers (%d)\n", config::CONNECTIONS_PER_PAIR);
	printf("\tt: TOS cutoff in bytes (%d)\n", config::TOS_CUTOFF);
	printf("\tm: Mean flow size in bytes (%d)\n", config::MEAN_FLOW_SIZE);
	printf("\tv: Verbose level (%d)\n", config::VERBOSE_LEVEL);
	printf("\th: Print this help and exit\n");
}

void init(int argc, char *argv[]) {
	int i;
	int c;

	rand_init();

	while ((c = getopt(argc, argv, "T:n:i:r:P:t:m:hv:")) != -1) {
		switch (c) {
		case 'T':
			config::TENANT_ID = atoi(optarg);
			break;

		case 'n':
			config::NUM_WORKERS = atoi(optarg);
			break;

		case 'i':
			config::ID = atoi(optarg);
			break;

		case 'r':
			config::MEAN_RATE_MBPS = atoi(optarg);
			break;

		case 'P':
			config::CONNECTIONS_PER_PAIR = atoi(optarg);
			break;

		case 't':
			config::TOS_CUTOFF = atoi(optarg);
			break;

		case 'm':
			config::MEAN_FLOW_SIZE = atoi(optarg);
			break;

		case 'v':
			config::VERBOSE_LEVEL = atoi(optarg);
			break;

		case 'h':
			usage(argv[0]);
			exit(0);
		}
	}

	REP(i, config::NUM_WORKERS) {
		if (config::TENANT_ID == -1) {
			sprintf(hosts[i], "192.168.2.%d", i+1);
		} else {
			sprintf(hosts[i], "11.0.%d.%d", config::TENANT_ID, i+1);
		}
	}

	set_nice(config::NICE_LEVEL);
}

void Receiver()
{
	char buff[32] = "0.0.0.0";
	if (config::TENANT_ID != -1)
		sprintf(buff, "11.0.%d.%d", config::TENANT_ID, config::ID + 1);

	server = new Server(buff, 6655, 4);
	server->run();
}

u64 delta_nsec(struct timespec &start, struct timespec &end) {
	return (end.tv_sec - start.tv_sec) * NSEC_PER_SEC + (end.tv_nsec - start.tv_nsec);
}

u64 time_nsec() {
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	return t.tv_sec * NSEC_PER_SEC + t.tv_nsec;
}

void LoadGenerator()
{
	/* Spawn parallel connections to other hosts */
	client = new Client(4);
	int idx = 0;
	client->set_stats(&fctstats);

	/* Dispatch work to connections */
	REP(i, config::NUM_WORKERS) {
		if (i == config::ID)
			continue;
		REP(j, config::CONNECTIONS_PER_PAIR)
			client->Connect(hosts[i], config::PORT, idx);
		idx += 1;
	}

	client->start_threads();

	/* Wait till everyone connects */
	int remaining_conns;
	const int target = (config::NUM_WORKERS - 1) * config::CONNECTIONS_PER_PAIR;
	do {

		remaining_conns = target;
		REP(to, config::NUM_WORKERS - 1)
			EACH(conn, client->connections[to])
			remaining_conns -= (*conn)->is_connected();
		printf("Connections %d/%d\n", target-remaining_conns, target);
		sleep(1);
	} while (remaining_conns > 0);

	/* Start the load generation */
	u64 now = time_nsec();
	u64 next = now;

	RandomExp rexp(config::MEAN_FLOW_SIZE);
	double mean_inter_usec = config::MEAN_FLOW_SIZE * 8.0 / config::MEAN_RATE_MBPS;
	printf("Mean inter flow time: %.3lfus\n", mean_inter_usec);
	RandomExp interus(mean_inter_usec);

	while (1) {
		int r, c, num;
		now = time_nsec();
		if (next >= now)
			continue;

		r = rand() % (config::NUM_WORKERS - 1);
		num = client->connections[r].size();

		/* Connections yet to be made */
		if (num != config::CONNECTIONS_PER_PAIR) {
			continue;
		}

		double sz = rexp.gen();
		c = rand() % num;
		client->connections[r][c]->submit_work(sz);
		next = now + (u64)(interus.gen() * 1000);
	}
}

void Stats() {
	while (1) {
		fctstats.print();
		sleep(1);
	}
}

int main(int argc, char *argv[]) {
	std::thread rx,tx,st;

	init(argc, argv);
	rx = std::thread(Receiver);
	tx = std::thread(LoadGenerator);
	st = std::thread(Stats);

	rx.join();
	tx.join();
	st.join();
	return 0;
}
