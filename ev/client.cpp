#include <vector>
#include <map>
#include <thread>

#include <netinet/in.h>
#include <ev.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netinet/tcp.h>

#include "connection.cpp"

#define MAX_THREADS (16)
#define LISTEN_BACKLOG (100)
#define MAX_HOSTS (20)

struct Client {
	int num_threads;
	std::thread *loopers[MAX_THREADS];
	std::vector<struct ev_loop *> thread_loop;
	std::vector<Connection *> connections[MAX_HOSTS];
	Client(int num_threads);
	FCTStats *stats;

	void ready_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);

	static void static_ready_cb(struct ev_loop *loop, struct ev_io *watcher, int revents) {
		Client *c = (Client *)(watcher->data);
		c->ready_cb(loop, watcher, revents);
	}

	void Connect(char *ip, int port, int id);

	bool check_dispatch();
	void dispatch_threads();
	void start_threads();
	void set_stats(FCTStats *s) {
		this->stats = s;
	}
};

Client::Client(int num_threads)
{
	this->num_threads = num_threads;
	REP(i, num_threads) {
		struct ev_loop *l = ev_loop_new(EVBACKEND_EPOLL);
		thread_loop.push_back(l);
	}
}

void Client::start_threads()
{
	REP(i, num_threads) {
		loopers[i] = new std::thread(thread_looper,
					     thread_loop[i], i);
	}
}

void Client::Connect(char *ip, int port, int id)
{
	static int next_thread;
	struct sockaddr_in addr;
	int sock;

	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_aton(ip, &addr.sin_addr);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		perror("socket()");
		exit(-1);
	}

	int optval = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *)&optval, sizeof(int));
	set_non_blocking(sock);

	/* Spawn the connect */
	connect(sock, (struct sockaddr *)&addr, sizeof(addr));
	printf("Connecting to [%d] %s:%d\n", id, ip, port);

	/* Wait... */
	Connection *c = new Connection(sock);
	connections[id].push_back(c);
	c->id = connections[id].size();
	c->addr = addr;
	c->attach(thread_loop[next_thread++]);
	c->set_stats(this->stats);
	next_thread %= num_threads;
}
