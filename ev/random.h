#include <stdlib.h>
#include <math.h>
#include <cassert>

const unsigned int seed = 0xdeadbeef;

struct Random {
	virtual double gen() { printf("Calling base class random.  Don't do this!\n"); return 0.0; }
	double operator()() {
		return gen();
	}
};

struct RandomConstant: public Random {
	double c;
	RandomConstant(double c): c(c) {}
	double gen() {
		return c;
	}
};

struct RandomUniform: public Random {
	double lo, hi;
	RandomUniform(double lo, double hi):lo(lo), hi(hi) {}
	double gen() {
		return lo + (hi - lo) * 1.0 * rand() / RAND_MAX;
	}
};

struct RandomExp: public Random {
	double mean;
	RandomUniform unif;
	RandomExp(double mean): mean(mean), unif(0, 1) {}
	double gen() {
		double u = unif();
		return log(u) * -mean;
	}
};

struct RandomPareto: public Random {
	double mean, alpha, xm;
	RandomUniform unif;
	RandomPareto(double mean, double alpha):mean(mean), alpha(alpha), unif(0, 1) {
		assert(alpha > 1.0);
		xm = (alpha - 1) * mean / alpha;
	}

	double gen() {
		double u = unif();
		return xm * pow(u, -1.0 / alpha);
	}
};

void rand_init() {
	srand(seed);
	// srand(time(NULL));
}
