#ifndef __CONNECTION_H__
#define __CONNECTION_H__

#include <ev.h>
#include <time.h>
#include <queue>
#include <atomic>

#define FLAGS_MESSAGE (1)
#define FLAGS_ACK (2)

struct flow_info {
	int bytes;
	int flags;
	u64 time;
}__attribute__((packed));

u64 time_nsec();

struct Connection
{
	int sock;
	struct ev_io ev;
	struct ev_timer timer;
	struct sockaddr_in addr;
	int id;
	int reconnect_tries;

	std::queue<int> queue;
	std::atomic<bool> lock;
	/* For send */
	bool new_flow;

	/* For receive */
	int to_receive;
	struct flow_info curr_info;

	/* stats */
	FCTStats *stats;

	Connection(int fd);
	void attach(struct ev_loop *loop);

	static void static_ready_cb(struct ev_loop *l, struct ev_io *w, int revents) {
		Connection *c = (Connection *)w->data;

		if (!c->is_connected())
			return;

		if (revents & EV_READ) {
			c->ready_read(l, w);
		}

		if (revents & EV_WRITE) {
			c->ready_write(l, w);
		}

		if (revents & EV_ERROR) {
			c->error(l, w);
		}
	}

	static void static_timeout_cb(struct ev_loop *l, struct ev_timer *w, int revents) {
		Connection *c = (Connection *)w->data;
		c->timeout(l, w);
	}

	void set_stats(FCTStats *s) {
		this->stats = s;
	}

	void ready_read(struct ev_loop *l, struct ev_io *w);
	void ready_write(struct ev_loop *l, struct ev_io *w);
	void error(struct ev_loop *l, struct ev_io *w);
	void timeout(struct ev_loop *l, struct ev_timer *w);

	bool is_connected() {
		struct sockaddr_in peeraddr;
		socklen_t len = sizeof(peeraddr);

		/* A listening socket is always connected... */
		if (addr.sin_addr.s_addr == INADDR_ANY)
			return true;

		if (getpeername(sock, (struct sockaddr*)&peeraddr, &len))
			return false;

		return true;
	}

	void reconnect() {
		connect(sock, (struct sockaddr *)&addr, sizeof(addr));
	}

	void submit_work(int bytes) {
		if (!is_connected())
			return;

		while (lock.exchange(true));
		if (queue.size() < config::MAX_WORK_BACKLOG) {
			queue.push(bytes);
		} else {
			stats::msgs_dropped++;
			stats::bytes_dropped += bytes;
		}
		lock = false;
	}

	void clear() {
		while (lock.exchange(true));
		queue = std::queue<int>();
		lock = false;
	}

	void send_ack();
};

Connection::Connection(int fd)
{
	sock = fd;
	ev.data = this;
	ev_io_init(&ev, Connection::static_ready_cb, fd,
		   EV_READ | EV_WRITE);

	timer.data = this;
	ev_timer_init(&timer, Connection::static_timeout_cb, 1.0, 0);

	new_flow = 0;
	to_receive = 0;
	bzero(&curr_info, sizeof(curr_info));

	reconnect_tries = 0;
	stats = NULL;
}

void Connection::attach(struct ev_loop *loop)
{
	ev_io_start(loop, &ev);
	ev_timer_start(loop, &timer);
}

void Connection::ready_read(struct ev_loop *l, struct ev_io *w)
{
	if (to_receive == 0) {
		if (recv(sock, (void *)&curr_info, sizeof(curr_info), 0) <= 0)
			return;

		to_receive = curr_info.bytes;
		if (curr_info.flags == FLAGS_ACK) {
			u64 now = time_nsec();
			u64 delta = now - curr_info.time;
			if (this->stats) {
				this->stats->insert(curr_info.bytes, delta * 1e-3);
			}
			//printf("Received ACK, %llu usec\n", (now - curr_info.time) / 1000);
			to_receive = 0;
			return;
		} else if (curr_info.flags == FLAGS_MESSAGE) {
			//printf("To receive %d bytes, flags %d\n", to_receive, curr_info.flags);
		} else {
			//printf("Ignoring garbage...\n");
			to_receive = 0;
		}
	} else {
		int canreceive = min(to_receive, config::BUFFER_SIZE);
		assert(canreceive > 0);
		int receive = recv(sock, (void *)config::BLOB, canreceive, 0);
		if (receive <= 0)
			return;
		to_receive -= receive;
		if (to_receive == 0)
			send_ack();
	}
}

void Connection::send_ack()
{
	//printf("Sending ACK: %d\n", curr_info.bytes);
	curr_info.flags = FLAGS_ACK;
	send(sock, (void *)&curr_info, sizeof(curr_info), 0);
	curr_info.flags = FLAGS_MESSAGE;
}

void Connection::ready_write(struct ev_loop *l, struct ev_io *w)
{
	struct flow_info info;

	if (!queue.size())
		return;

	int &bytes = queue.front();
	if (new_flow) {
		//printf("Sending new flow %d bytes\n", bytes);
		info.bytes = bytes;
		info.flags = FLAGS_MESSAGE;
		info.time = time_nsec();
		if (send(sock, (void *)&info, sizeof(info), 0) < 0)
			return;
		new_flow = false;
	}

	int cansend = min(bytes, config::BUFFER_SIZE);
	int tos = 0x24;

	if (bytes < config::TOS_CUTOFF) {
		setsockopt(sock, IPPROTO_IP, IP_TOS, &tos, sizeof(tos));
	} else {
		tos = 0;
		setsockopt(sock, IPPROTO_IP, IP_TOS, &tos, sizeof(tos));
	}

	int ret = write(sock, config::BLOB, cansend);
	//printf("%s: write(%d, %d) => %d\n", __FUNCTION__, sock, cansend, ret);

	if (ret > 0) {
		bytes -= ret;
		if (bytes == 0) {
			while (lock.exchange(true));
			queue.pop();
			lock = false;
			new_flow = true;
		}
	}
}

void Connection::error(struct ev_loop *l, struct ev_io *w)
{

}

void Connection::timeout(struct ev_loop *l, struct ev_timer *w)
{
	if (!is_connected()) {
		reconnect();
		printf("Reconnecting %d...%s:%d\n",
		       reconnect_tries++,
		       inet_ntoa(addr.sin_addr), id);
		to_receive = 0;
	} else {
		reconnect_tries = 0;
	}

	//printf("Queue[%d] remaining: %d\n", id, queue.size());

	ev_timer_set(&timer, 2, 0);
	ev_timer_start(l, &timer);
}

#endif /* __CONNECTION_H__ */
