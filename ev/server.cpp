#include <vector>
#include <map>
#include <thread>

#include <netinet/in.h>
#include <ev.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include "connection.cpp"

#define MAX_THREADS (16)
#define LISTEN_BACKLOG (100)

struct Server {
	struct sockaddr_in addr;
	int port;
	int sock_listen;
	struct ev_loop *loop;
	struct ev_io w_accept;

	std::vector<int> client_fds;
	std::map<std::string, int> connections;

	int num_threads;
	std::thread *loopers[MAX_THREADS];
	std::vector<struct ev_loop *> thread_loop;

	Server(char *ip, int port, int num_threads);

	void run(int restart=0) {
		do {
			ev_run(loop, 0);
		} while (restart);
	}

	void accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);
	void ready_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);

	static void static_accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents) {
		Server *s = (Server *)(watcher->data);
		s->accept_cb(loop, watcher, revents);
	}

	static void static_ready_cb(struct ev_loop *loop, struct ev_io *watcher, int revents) {
		Server *s = (Server *)(watcher->data);
		s->ready_cb(loop, watcher, revents);
	}

	bool check_dispatch();
	void dispatch_threads();
};

Server::Server(char *ip, int port, int num_threads) {
	int addr_len = sizeof(addr);

	loop = ev_default_loop(0);
	bzero(&addr, sizeof(addr));

	this->port = port;
	this->num_threads = num_threads;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_aton(ip, &addr.sin_addr);

	sock_listen = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_listen < 0) {
		perror("socket()");
		exit(-1);
	}

	int optval = 1;
	setsockopt(sock_listen, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

	set_non_blocking(sock_listen);

	if (bind(sock_listen, (struct sockaddr *)&addr, addr_len) != 0) {
		perror("bind()");
		exit(-1);
	}

	if (listen(sock_listen, LISTEN_BACKLOG) < 0) {
		perror("listen()");
		exit(-1);
	}

	w_accept.data = this;
	/* Register for connections */
	ev_io_init(&w_accept,
		   &Server::static_accept_cb,
		   sock_listen, EV_READ);
	ev_io_start(loop, &w_accept);
}

void Server::accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents)
{
	struct sockaddr_in client_addr;
	int sock_client;
	socklen_t client_len;

	if (revents & EV_ERROR) {
		printf("Error on the accept socket.\n");
		return;
	}

	bzero(&client_addr, sizeof(client_addr));
	client_len = sizeof(client_addr);
	sock_client = accept(watcher->fd, (struct sockaddr *)&client_addr, &client_len);

	if (sock_client < 0) {
		perror("accept()");
		return;
	}

	char addr[128];
	strcpy(addr, inet_ntoa(client_addr.sin_addr));
	printf("Accepted from %s:%d\n",
	       addr,
	       htons(client_addr.sin_port));

	connections[addr]++;
	client_fds.push_back(sock_client);

	if (check_dispatch()) {
		/* All done.  Start the connection loop! */
		printf("All done.  Starting server connection loop (Reader)!\n");
		dispatch_threads();
	}
}

void Server::ready_cb(struct ev_loop *loop, struct ev_io *watcher, int revents)
{
	/* Not implemented... The server only accepts connections... */
}

bool Server::check_dispatch()
{
	bool done = true;
	int num = 0;

	EACH(it, connections) {
		if (it->second < config::CONNECTIONS_PER_PAIR) {
			return false;
		}
		num++;
	}

	return num == config::NUM_WORKERS - 1;
}

static void thread_looper(struct ev_loop *loop, int threadid) {
	printf("Thread id %d %s:%d\n", threadid, __FILE__, __LINE__);
	do {
		ev_run(loop, 0);
	} while (1);
}

void Server::dispatch_threads()
{
	int next_thread = 0;

	REP(i, num_threads) {
		struct ev_loop *ev = ev_loop_new(EVBACKEND_EPOLL);
		thread_loop.push_back(ev);
	}

	EACH(it, client_fds) {
		int fd = *it;
		Connection *c = new Connection(fd);
		c->attach(thread_loop[next_thread++]);
		if (next_thread >= num_threads)
			next_thread = 0;
	}

	printf("Spawning all threads...\n");
	REP(i, num_threads) {
		loopers[i] = new std::thread(thread_looper, thread_loop[i], i);
	}
}
