
#include <strings.h>
#include <atomic>

using std::min;
using std::max;

#define BINS (4)
#define HIST_POWERS (4)
#define BUCKETS (10)
#define MIN_FCT_USEC (30.0)

struct Histogram {
	u64 count[HIST_POWERS][BUCKETS];
	u64 total_count;
	double start_del;

	Histogram(double start=1) {
		double curr = start, del = start;
		bzero(count, sizeof(count));
		total_count = 0;
		start_del = start;
	}

	void insert(double value) {
		value /= start_del;
		int power = 0;
		while (not (0 <= value and value < 10)) {
			value /= 10;
			power++;
		}
		count[power][int(value)]++;
		total_count++;
	}

	void print(int factor=1, const char *suffix="") {
		double min_frac = 10;
		double frac[HIST_POWERS][BUCKETS];
		double sum = 0.0;
		double value = start_del;
		double del = start_del;

		REP(i, HIST_POWERS) {
			REP(j, BUCKETS) {
				if (count[i][j] == 0)
					continue;
				frac[i][j] = count[i][j] * 1.0/total_count;
				if (frac[i][j] > 1e-2)
					min_frac = min(min_frac, frac[i][j]);
			}
		}

		REP(i, HIST_POWERS) {
			REP(j, BUCKETS) {
				int num = min(20, int(frac[i][j]/min_frac));
				if (count[i][j] == 0 or num <= 0)
					continue;
				sum += frac[i][j];
				printf("[%.3lf, %.3lf)%s => %.4lf, %.4lf  ",
				       value/factor, (value + del)/factor, suffix,
				       frac[i][j], sum);
				REP(j, num)
					printf("#");
				printf("\n");
				value += del;
			}
			del *= 10;
			value = del;
		}
	}

	void clear() {
		total_count = 0;
		bzero(count, sizeof(count));
	}
};

struct FCTStats {
	double rate_mbps;
	Histogram ratio_bins[BINS];
	Histogram fct_bins[BINS];
	u32 flowsize_cutoff[BINS];
	int num_cutoffs;
	std::atomic<bool> lock;

	FCTStats(double r=10000):rate_mbps(r) {
		flowsize_cutoff[0] = 100 * 1000;
		flowsize_cutoff[1] = 10 * 1000 * 1000;
		flowsize_cutoff[2] = 100 * 1000 * 1000;
		flowsize_cutoff[3] = ~0;
		num_cutoffs = 3;

		/* for FCTs start with 100us */
		REP(i, BINS) fct_bins[i].start_del = 100;
	}

	void insert(int flowsize, double fct_usec) {
		double best_usec = max(8. * flowsize / rate_mbps, MIN_FCT_USEC);
		double ratio = fct_usec / best_usec;

		if (ratio < 0.5) {
			printf("Something seriously wrong: flowsize %d, fct: %.3lf, ratio: %.3lf\n",
			       flowsize, fct_usec, ratio);
			exit(-1);
		}

		if (config::VERBOSE_LEVEL > 100) {
			printf("Flowsize: %d completed in %.3lfus, ratio: %.3lf\n",
			       flowsize, fct_usec, ratio);
		}

		while (lock.exchange(true));

		REP(i, num_cutoffs) {
			if (flowsize <= flowsize_cutoff[i]) {
				ratio_bins[i].insert(ratio);
				fct_bins[i].insert(fct_usec);
				break;
			}
		}

		lock = false;
	}

	void print() {
		printf("************* Ratio to ideal *******************\n");
		REP(i, num_cutoffs) {
			printf("----------- <= %d kB -----------\n", flowsize_cutoff[i]/1000);
			ratio_bins[i].print();
		}

		printf("************* Actual FCT values *******************\n");
		REP(i, num_cutoffs) {
			printf("----------- <= %d kB -----------\n", flowsize_cutoff[i]/1000);
			fct_bins[i].print(1000, "ms");
		}
	}

	void clear() {
		REP(i, num_cutoffs) {
			fct_bins[i].clear();
			ratio_bins[i].clear();
		}
	}
};
