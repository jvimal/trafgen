package main

import (
	"code.google.com/p/mxk/go1/flowcontrol"
	"flag"
	"log"
	"net"
	"time"
)

const (
	// Number of bytes to write into the socket in one go.  Can't
	// be more than the MTU if protocol is UDP.
	WRITE_SIZE = 1400
)

// Flags.  Feel free to add more.
var dest = flag.String("dest", "", "Destination IP/hostname:port")
var onmsec = flag.Int("onmsec", 5, "On duration millisecond")
var offmsec = flag.Int("offmsec", 20, "Off duration millisecond")

// This rate is the application-level write rate into the socket
// during the ON period.
var rate = flag.Int("rate", 100, "Socket write rate (bytes/second)")
var proto = flag.String("proto", "udp", "TCP or UDP protocol")

// The temporary contents that we write into the socket.  It's all 0s
// but you can fill this up with something.
var buf = make([]byte, WRITE_SIZE)

// Main traffic generating routine.
func SendTraffic(dest string) {
	conn, err := net.Dial(*proto, dest)
	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	// rate limited writer
	rwriter := flowcontrol.NewWriter(conn, (int64)(*rate))
	defer rwriter.Close()

	// http://blog.golang.org/go-concurrency-patterns-timing-out-and
	timeout := make(chan bool, 1)

	for {
		// Timeout call after onmsec.
		go func() {
			time.Sleep(time.Duration(*onmsec) * time.Millisecond)
			timeout <- true
		}()

		// Keep sending traffic for at most onmsec at the
		// requested rate
		done := false
		for !done {
			// We haven't timed out, so keep writing into
			// the connection, until it signals an error
			_, err := rwriter.Write(buf)
			if err != nil {
				done = true
				break
			}

			select {
			case <-timeout:
				// We have timed out, so break out of the for loop.
				done = true
				break
			default:
				// If there is nothing on the channel
				// (i.e., no time out), continue with
				// the for-loop sending traffic.
			}
		}

		// Sleep for offmsec
		time.Sleep(time.Duration(*offmsec) * time.Millisecond)
	}
}

// Example invocation:
// ./tgen -dest 8.8.8.8:5001 -onmsec 1 -offmsec 10 -rate 10000000
func main() {
	flag.Parse()
	log.Printf("Sending traffic to `%s'", *dest)
	SendTraffic(*dest)
}
